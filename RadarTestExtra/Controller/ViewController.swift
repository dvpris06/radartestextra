//
//  ViewController.swift
//  RadarTestExtra
//
//  Created by dragdimon on 19/02/2020.
//

import UIKit

class ViewController: UIViewController {

    //MARK: Properties
    
    let planeImage = UIImage(named: "plane")
    var biasForAngle = [Float]()
    var circleSizes = [CircleSize]()
    var frames = [CGRect]()
    var busySectors = Array(repeating: [Int](), count: Constants.circlesNumber)
    
    //MARK: VC Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawCircles(number: Constants.circlesNumber)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setBiases()
        setCircleSizes()
        drawPlanesWithoutOverlaps(number: Constants.planesNumber)
    }

    //MARK: Utility Functions
    
    // Устанавливаем смещения угла для каждого круга
    private func setBiases() {
        
        for _ in 1...Constants.circlesNumber {
            biasForAngle.append(Float.random(in: 0...Float.pi))
        }
    }
    
    private func setCircleSizes() {
        
        for index in 0..<Constants.circlesNumber {
            // Делим круг на сектора, перекрываемые изображением самолета
            // и рассчитываем необходимые размеры
            let circle = view.subviews[index]
            let radius = Float(circle.frame.size.height - 2 * Constants.circleLineWidth) / 2
            let circumference = 2 * Float.pi * radius
            let numberOfSectors = Int(circumference / Constants.diameterOfPlane)
            circleSizes.append(CircleSize(radius: radius, circumference: circumference, numberOfSectors: numberOfSectors))
        }
    }
    
    private func setCoordinates(sector: Int,
                                numberOfSectors: Int,
                                circleNumber: Int,
                                radius: Float,
                                center: CGPoint) -> CGPoint {
        // Устанавливаем угол для случайного сектора и добавляем смещение
        let alpha = Float(sector) * 2 * Float.pi / Float(numberOfSectors) + biasForAngle[circleNumber]
        var x = CGFloat(radius * cos(alpha)) + center.x
        var y = CGFloat(radius * sin(alpha)) + center.y
        
        // Определяем к какой четверти круга относится точка и сдвигаем координаты изображения
        if x > center.x {
            if y < center.y {
                x -= Constants.planesSize
            } else {
                x -= Constants.planesSize
                y -= Constants.planesSize
            }
        } else if y > center.y {
            y -= Constants.planesSize
        }

        return CGPoint(x: x, y: y)
    }
    
    //MARK: Draw Functions
    
    private func drawCircles(number: Int ){
        var circleWidth = Constants.circleWidth
        var circleHeight = circleWidth
        
        for _ in 1...number {
            let circleView = CircleView(frame: CGRect(x: view.center.x - circleWidth/2,
                                                      y: view.center.y - circleHeight/2,
                                                      width: circleWidth,
                                                      height: circleHeight))
            view.addSubview(circleView)
            
            // Увеличиваем размеры для следующего круга
            circleWidth += Constants.deltaCircleWidth
            circleHeight = circleWidth
        }
    }
    
    private func drawPlanesWithoutOverlaps(number: Int) {
        
        for _ in 1...number {
            // Выбираем случайный круг и сектор в нем
            let circleNumber = Int.random(in: 0..<Constants.circlesNumber)
            let circleSize = circleSizes[circleNumber]
            var sector = Int.random(in: 0..<circleSize.numberOfSectors)
            
            // Устанавливаем координаты для размещения изображения
            var coordinates = setCoordinates(sector: sector,
                                             numberOfSectors: circleSize.numberOfSectors,
                                             circleNumber: circleNumber,
                                             radius: circleSize.radius,
                                             center: view.center)
  
            // Определяем рамку для изображения
            var frame = CGRect(x: coordinates.x,
                               y: coordinates.y,
                               width: Constants.planesSize,
                               height: Constants.planesSize)
            
            // Если массив, содержащий рамки установленных изображений не пуст и есть свободные сектора на круге,
            // проверяем есть ли пересечения с другими изображениями
            // и переставляем изображение в следующий сектор, если условие истинно
            if !frames.isEmpty && (busySectors[circleNumber].count < circleSize.numberOfSectors) {
                
                for rect in frames {
                    if frame.intersects(rect) {
                        
                        while busySectors[circleNumber].contains(sector) {
                            sector += 1
                            
                            // Сдвигаем в случае выхода за границы
                            if sector >= circleSize.numberOfSectors {
                                sector -= circleSize.numberOfSectors
                            }
                        }
                        
                        // Устанавливаем обновленные координаты
                        coordinates = setCoordinates(sector: sector,
                                                     numberOfSectors: circleSize.numberOfSectors,
                                                     circleNumber: circleNumber,
                                                     radius: circleSize.radius,
                                                     center: view.center)
                     
                        frame = CGRect(x: coordinates.x,
                                           y: coordinates.y,
                                           width: Constants.planesSize,
                                           height: Constants.planesSize)
                        
                    }
                }
            }
            
            // Размещаем imageView по рассчитанным координатам
            let planeImageView = UIImageView(frame: frame)
            planeImageView.image = planeImage
            view.addSubview(planeImageView)
            
            // Обновляем информацию о рамках установленных изображений и занятых секторах
            frames.append(frame)
            busySectors[circleNumber].append(sector)
            
        }
    }
}

