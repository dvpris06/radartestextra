//
//  Constants.swift
//  RadarTestExtra
//
//  Created by dragdimon on 19/02/2020.
//

import UIKit

struct Constants {
    
    static let circlesNumber = 10
    static let planesNumber = 100
    static let circleLineWidth: CGFloat = 1
    static let circleWidth: CGFloat = 40
    static let deltaCircleWidth: CGFloat = 35
    static let planesSize: CGFloat = 15
    static let diameterOfPlane = Float(2 * Constants.planesSize) * sqrtf(2)
}
