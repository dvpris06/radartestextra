//
//  CircleSize.swift
//  RadarTestExtra
//
//  Created by dragdimon on 19/02/2020.
//

import Foundation

struct CircleSize {
    
    var radius: Float
    var circumference: Float
    var numberOfSectors: Int
}
