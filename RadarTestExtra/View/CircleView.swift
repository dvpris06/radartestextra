//
//  CircleView.swift
//  RadarTestExtra
//
//  Created by dragdimon on 19/02/2020.
//

import UIKit

class CircleView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        
        if let context = UIGraphicsGetCurrentContext() {
            
            // Устанавливаем ширину линии
            context.setLineWidth(Constants.circleLineWidth)
            
            // Устанавливаем цвет линии
            UIColor.black.set()
            
            // Создаем круг
            let center = CGPoint(x: rect.midX, y: rect.midY)
            let radius = (rect.width - Constants.circleLineWidth) / 2
            context.addArc(center: center, radius: radius, startAngle: 0.0, endAngle: .pi * 2.0, clockwise: true)
                
            // Рисуем круг
            context.strokePath()
        }
    }

}
